export default function validarCrearCuenta(valores){

    let errores = {};

    // Validar el nombre de usuario
    if(!valores.nombre){
        errores.nombre = "El nombre es obligatorio";
    }

    // Validar nombre empresa
    if(!valores.empresa){
        errores.empresa = "El nombre de la empresa es obligatorio";
    }

    // Validar la URL
    if (!valores.url){
        errores.url = "La URL del producto es necesaria";
    }else if(!/^(ftp|http|https):\/\/[^ "]+$/.test(valores.url)){
        errores.url = "URL no válida";
    }

    // Validar la descripción
    if(!valores.descripcion){
        errores.descripcion = "Es necesaria una descripción del producto";
    }


    return errores;
}