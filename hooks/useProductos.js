import React, {useState, useEffect, useContext} from 'react';
import {FirebaseContext} from '../firebase';

const useProductos = (orden) => {

    const [productos, setProductos] = useState([]);

    const { firebase } = useContext(FirebaseContext);
  
    useEffect(() => {
      
      const obtenerProductos = () => {
        firebase.db.collection('productos').orderBy(orden, 'desc').onSnapshot(manejarSnapShot)
      }
      obtenerProductos();
    },[])
  
    function manejarSnapShot(snapshot) {
      const productosBbdd = snapshot.docs.map(doc => {
  
        return {
          id: doc.id,
          ...doc.data()
        }
      })
  
      setProductos(productosBbdd);
    }
    return{
        productos
    }
};

export default useProductos;

