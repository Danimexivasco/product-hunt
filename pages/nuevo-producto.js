import React, { useState, useContext } from 'react';
import { css } from '@emotion/core';
import Router from 'next/router';
import FileUploader from 'react-firebase-file-uploader';
import Layout from '../components/layout/Layout';
import { Formulario, Campo, InputSubmit, Error } from '../components/ui/Formulario';
import Error404 from '../components/layout/404';

import { FirebaseContext } from '../firebase';

// Validaciones
import useValidacion from '../hooks/useValidacion';
import validarCrearProducto from '../validacion/validarCrearProducto';

const STATE_INICIAL = {
  nombre: '',
  empresa: '',
  imagen: '',
  url: '',
  descripcion: ''
}

const NuevoProducto = () => {

  // State de las imagenes
  const [nombreImagen, setNombreImagen] = useState('');
  const [subiendo, setSubiendo] = useState(false);
  const [progreso, setProgreso] = useState(0);
  const [urlImagen, setUrlImagen] = useState('');

  const [error, setError] = useState(false)

  const { valores, errores, handleChange, handleSubmit } = useValidacion(STATE_INICIAL, validarCrearProducto, crearProducto);

  const { nombre, empresa, imagen, url, descripcion } = valores;

  // Context con las operaciones CRUD de FIREBASE
  const { usuario, firebase } = useContext(FirebaseContext);
  // console.log(usuario);

  async function crearProducto() {

    // Si el usuario no esta autenticado llevarlo al login
    if (!usuario) {
      return Router.push('/login');
    }

    // Crear el objeto de nuevo producto
    const producto = {
      nombre,
      empresa,
      url,
      urlImagen,
      descripcion,
      votos: 0,
      comentarios: [],
      creado: Date.now(),
      creador: {
        id: usuario.uid,
        nombre: usuario.displayName
      },
      haVotado: []
    }

    // Insertarlo en la bbdd
    firebase.db.collection('productos').add(producto);

    return Router.push('/');
  }

  // Funciones de imagenes
  const handleUploadStart = () => {
    setProgreso(0);
    setSubiendo(true);
  };

  const handleProgress = progreso => {
    setProgreso({ progreso });
  };

  const handleUploadError = error => {
    setSubiendo(error);
    console.error(error);
  };

  const handleUploadSuccess = nombre => {
    setProgreso(100);
    setSubiendo(false);
    setNombreImagen(nombre)
    firebase
        .storage
        .ref('productos')
        .child(nombre)
        .getDownloadURL()
        .then(url => {
          console.log(url);
          setUrlImagen(url);
        });
  };

  

  return (
    <div>
      <Layout>
        { !usuario ? <Error404 msg= "Tienes que loguearte para poder ver esta sección" /> : (
          <>
          <h1
            css={css`
              text-align: center;
              margin-top: 5rem;
            `}
          >Nuevo Producto</h1>
          <Formulario
            onSubmit={handleSubmit}
            noValidate
          >
            <fieldset>
              <legend>Información General</legend>

              <Campo>
                <label htmlFor="nombre">Nombre</label>
                <input
                  type="text"
                  id="nombre"
                  placeholder="Nombre del producto"
                  name="nombre"
                  value={nombre}
                  onChange={handleChange}
                />
              </Campo>
              {errores.nombre && <Error>{errores.nombre}</Error>}

              <Campo>
                <label htmlFor="empresa">Empresa</label>
                <input
                  type="text"
                  id="empresa"
                  placeholder="Empresa o Compañía"
                  name="empresa"
                  value={empresa}
                  onChange={handleChange}
                />
              </Campo>
              {errores.empresa && <Error>{errores.empresa}</Error>}

              <Campo>
                <label htmlFor="imagen">Imagen</label>
                <FileUploader
                  accept='image/*'
                  id="imagen"
                  name="imagen"
                  randomizeFilename
                  storageRef={firebase.storage.ref('productos')}
                  onUploadStart={handleUploadStart}
                  onUploadError={handleUploadError}
                  onUploadSuccess={handleUploadSuccess}
                  onProgress={handleProgress}
                />
              </Campo>

              <Campo>
                <label htmlFor="url">URL</label>
                <input
                  type="text"
                  id="url"
                  name="url"
                  placeholder="URL del producto"
                  value={url}
                  onChange={handleChange}
                />
              </Campo>
              {errores.url && <Error>{errores.url}</Error>}

            </fieldset>

            <fieldset>
              <legend>Sobre tu Producto</legend>

              <Campo>
                <label htmlFor="descripcion">Descripción</label>
                <textarea
                  id="descripcion"
                  name="descripcion"
                  value={descripcion}
                  onChange={handleChange}
                />
              </Campo>
              {errores.descripcion && <Error>{errores.descripcion}</Error>}

            </fieldset>

            {error && <Error>{error}</Error>}
            <InputSubmit
              type="submit"
              value="Crear Producto"
            />

          </Formulario>
        </>

        )}
        
      </Layout>
    </div>
  );
}

export default NuevoProducto;
