


import React, { useEffect, useContext, useState } from 'react';
import { useRouter } from 'next/router';
import Error404 from '../../components/layout/404';
import Layout from '../../components/layout/Layout';
import { css } from '@emotion/core';
import styled from '@emotion/styled';
import formatDistanceToNow from 'date-fns/formatDistanceToNow';
import { es } from 'date-fns/locale';
import { Campo, InputSubmit } from '../../components/ui/Formulario';
import Boton from '../../components/ui/Boton';

import { FirebaseContext } from '../../firebase';


const ContenedorProducto = styled.div`
    @media(min-width:768px){
        display:grid;
        grid-template-columns: 2fr 1fr;
        column-gap: 2rem;
    }
`;

const CreadorProducto = styled.p`
    padding: .5rem 2rem;
    background-color: #da552f;
    color: #FFF;
    text-transform: uppercase;
    font-weight: bold;
    display: inline-block;
    text-align: center;
    margin:0;
`;


const Producto = () => {

    // State del componenete
    const [producto, setProducto] = useState({});
    const [error, setError] = useState(false);
    const [comentario, setComentario] = useState({
        mensaje: '',
        usuarioId: '',
        usuarioNombre: ''
    });
    const [consultarDB, setConsultarDB] = useState(true)

    // Routing para obtener el ID actual
    const router = useRouter();
    const { query: { id } } = router;

    // Context de Firebase
    const { firebase, usuario } = useContext(FirebaseContext);

    useEffect(() => {
        // Declaramos variable para trackear
        let estaCancelado = false;

        if (id && consultarDB) {
            const obtenerProducto = async () => {
                const productoQuery = await firebase.db.collection('productos').doc(id);
                const producto = await productoQuery.get();

                // Se ejecuta solo si esta montado
                if (!estaCancelado) {
                    if (producto.exists) {
                        setProducto(producto.data());
                    } else {
                        setError(true);
                    }
                    setConsultarDB(false);
                }

            }
            obtenerProducto();
        }
        return () => {
            // Ponemos la variable a true para evitar que intente actualizar un state inexistente
            estaCancelado = true;
        }
    }, [id])

    // if (Object.keys(producto).length === 0) return <p>Cargando...</p>

    const { creado, descripcion, empresa, comentarios, nombre, url, urlImagen, votos, creador, haVotado } = producto;

    // Administrar y valdiar los votos
    const votarProducto = () => {
        if (!usuario) {
            return router.push('/login')
        }

        // Obtener y sumar un nuevo voto
        const nuevoTotal = votos + 1;

        // Verificar si el usuario actual ha votado
        if (haVotado.includes(usuario.uid)) return;

        // Guardar el ID del usuario que ha votado
        const nuevoHaVotado = [...haVotado, usuario.uid];

        // Actualiza en la bbdd
        firebase.db.collection('productos').doc(id).update({
            votos: nuevoTotal,
            haVotado: nuevoHaVotado
        });

        // Actualizar el state
        setProducto({
            ...producto,
            votos: nuevoTotal
        })
    }

    // Identificar si el comentario es del creador del producto
    const esCreador = id => {
        if (creador.id == id) {
            return true;
        }
    }

    // Funciones para crear comentarios 
    const comentarioChange = e => {
        e.preventDefault();
        setComentario({
            ...comentario,
            [e.target.name]: e.target.value,
            usuarioId: usuario.uid,
            usuarioNombre: usuario.displayName
        })
    }

    const agregarComentario = e => {
        e.preventDefault();

        if (!usuario) {
            return router.push('/login');
        }

        if (comentario.mensaje.trim() === '') {
            return;
        }

        // Informacion extra al comentario
        // setComentario({
        //     ...comentario,
        //     usuarioId: usuario.uid,
        //     usuarioNombre: usuario.displayName
        // })

        // comentario.usuarioId = usuario.uid;
        // comentario.usuarioNombre = usuario.displayName;

        // Tomar copia de comentarios y agregarlos al array
        const nuevosComentarios = [...comentarios, comentario];


        // Actualizar la bbdd

        firebase.db.collection('productos').doc(id).update({
            comentarios: nuevosComentarios
        })


        // Actualiza el state
        setProducto({
            ...producto,
            comentarios: nuevosComentarios
        })

        setComentario({
            mensaje: '',
            usuarioId: '',
            usuarioNombre: ''
        })

        setConsultarDB(true);
    }

    // funcion que revisa que el creador del producto sea el mismo que esta autenticado
    const puedeBorrar = () => {
        if (!usuario) return false;

        if (creador.id === usuario.uid) {
            return true;
        }
    }

    const eliminarProducto = async () => {
        if (!usuario) {
            return router.push('/login');
        }

        if (creador.id !== usuario.uid) {
            return router.push('/login');
        }
        try {
            await firebase.db.collection('productos').doc(id).delete();
            router.push('/');
        } catch (error) {
            console.log(error);
        }
    }

    return (
        <Layout>
            {/* {error && <Error404 msg="Ooops... Parece que hay algún problema, aquí no hay nada que ver!" />} */}
            {error ? <Error404 msg="Ooops... Parece que hay algún problema, aquí no hay nada que ver!" /> : Object.keys(producto).length === 0 ? <p>Cargando...</p> : (
                <>

                    <div className="contenedor">
                        <h1
                            css={css`
                                text-align: center;
                                margin-top: 5rem;
                            `}
                        >{nombre}</h1>
                        <ContenedorProducto>
                            <div>

                                <p>Publicado hace: {formatDistanceToNow(new Date(creado), { locale: es })}</p>
                                <p>Autor: {creador.nombre}</p>


                                <img src={urlImagen} />
                                <p>{descripcion}</p>


                                {usuario && (
                                    <>
                                        <h2>Agrega tu comentario:</h2>
                                        <form
                                            onSubmit={agregarComentario}
                                        >
                                            <Campo>
                                                <input
                                                    type="text"
                                                    name="mensaje"
                                                    onChange={comentarioChange}
                                                    value={comentario.mensaje}
                                                />
                                            </Campo>
                                            <InputSubmit
                                                type="submit"
                                                value="Agregar Comentario"
                                            />
                                        </form>
                                        <h2
                                            css={css`
                                                margin:2rem 0;
                                            `}
                                        >Comentarios</h2>
                                        {comentarios.length === 0 ? <p>Aun no hay comentarios</p> : (
                                            <ul>
                                                {comentarios.map((comentario, i) => (
                                                    <li
                                                        key={`${comentario.usuarioId}-${i}`}
                                                        css={css`
                                                        border: 1px solid #e1e1e1;
                                                        padding: 2rem;
                                                    `}
                                                    >
                                                        <p>{comentario.mensaje}</p>
                                                        <p>Escrito por:
                                                        <span
                                                                css={css`
                                                                font-weight: bold;
                                                            `}
                                                            >{' '}{comentario.usuarioNombre}</span>
                                                        </p>
                                                        {esCreador(comentario.usuarioId) &&
                                                            <CreadorProducto>Creador</CreadorProducto>
                                                        }
                                                    </li>
                                                ))}
                                            </ul>
                                        )}

                                    </>
                                )}
                            </div>
                            <aside>
                                <p
                                    css={css`
                                        text-align:center;
                                    `}
                                >Empresa: {empresa}</p>
                                <Boton
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    bgColor={true}
                                    href={url}
                                >Visitar URL</Boton>

                                <p
                                    css={css`
                                        text-align: center;
                                        margin-top:5rem;
                                    `}
                                >{votos} Votos</p>
                                {usuario && (
                                    <Boton
                                        onClick={() => votarProducto()}
                                    >Votar</Boton>)
                                }
                            </aside>

                        </ContenedorProducto>

                        {puedeBorrar() &&
                            <Boton
                                onClick={eliminarProducto}
                            >Eliminar Producto</Boton>}
                    </div>

                </>
            )}


        </Layout>
    );
}

export default Producto
