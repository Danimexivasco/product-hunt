import React, { useEffect, useState } from 'react';
import Layout from '../components/layout/Layout';
import { useRouter } from 'next/router';
import useProductos from '../hooks/useProductos';
import DetallesProducto from '../components/layout/DetallesProducto';

const Buscar = () => {

    const router = useRouter();
    const { query: { q } } = router;
   

    // Destructuring de useProductos
    const { productos } = useProductos('creado');

    // State de resultado
    const [resultados, setResultados] = useState([]);

    useEffect(() => {
        const busqueda = q.toLowerCase();
        const busquedaSinTildes = quitarTildes(busqueda);
        const filtro = productos.filter(producto => {
            return (
                quitarTildes(producto.nombre.toLowerCase()).includes(busquedaSinTildes) ||
                quitarTildes(producto.descripcion.toLowerCase()).includes(busquedaSinTildes)
            )
        })
        setResultados(filtro)
        
    }, [q, productos])

    const quitarTildes = (palabra) => {
        // palabra = palabra.replace("á", "a");
        // palabra = palabra.replace("é", "e");
        // palabra = palabra.replace("í", "i");
        // palabra = palabra.replace("ó", "o");
        // palabra = palabra.replace("ú", "u");

        const letras = { 'á': 'a', 'é': 'e', 'í': 'i', 'ó': 'o', 'ú': 'u' };
        palabra = palabra.replace(/[áéíóú]/g, m => letras[m]);

        return palabra
    }

    return (
        <div>
            <Layout>
                {resultados.length <= 0 ? <p>No se han encontrado productos con esa búsqueda</p> : (
                    <div className="listado-productos">
                        <div className="contenedor">
                            <ul className="bg-white">
                                {resultados.map(producto => {

                                    return (
                                        <DetallesProducto
                                            key={producto.id}
                                            producto={producto}
                                        />
                                    )
                                })}
                            </ul>
                        </div>
                    </div>
                )}
            </Layout>

        </div>

    );
}

export default Buscar;